// Guardamos el boton de HTML en una variable de JS
let boton = document.querySelector("#login-button");

// Imprimimos en la consola el contenido de la variable (boton)
console.log(boton);

// relacionamos el boton con la funcion
boton.setAttribute("onclick","validarUsuario()");




// Esta es la funcion para validar el nombre de usuario y contrase;a
function validarUsuario(){
    console.log("Estamos dentro de la funcion");

    // Este es el usuario registrado
    const USUARIO_REGISTRADO = "Pedro";
    const CLAVE_REGISTRADO = "12345";

    //Estos son los datos ingresados en el fomulario
    let miClave = document.querySelector("#pass").value;
    let miUsuario = document.querySelector("#user").value;

    if (USUARIO_REGISTRADO == miUsuario && CLAVE_REGISTRADO == miClave) {
        //Este es el camino verdadero
        //Aca nos deja pasar
        console.log("Ingresaste");

        //Agrega class para que de una efecto visual
        document.querySelector("#contenedor").className += " form-success";
        
        //Ocualta el formulario
        document.querySelector("form").style.display = "none";
    } else {
        //Este es el camino de lo falso
        //Aca no nos dejo pasar
        console.log("A la cucha!!");
    }

}